class ListFilterParam {
  String description;
  String location;
  int provider;

  ListFilterParam({
    this.description,
    this.location,
    this.provider,
  });

  factory ListFilterParam.getDefault() {
    return ListFilterParam(
      provider: 0,
      location: null,
      description: null,
    );
  }
}
