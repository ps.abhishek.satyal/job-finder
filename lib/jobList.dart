import 'package:flutter/material.dart';
import 'package:job_finder/jobDetails.dart';

import 'job.dart';

class JobList extends StatelessWidget {
  List<Job> jobList;

  JobList(this.jobList);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: ListView.builder(
        itemCount: jobList.length,
        itemBuilder: (context, index) {
          final item = jobList[index];

          return ListTile(
            title: buildTitle(title: item.title),
            subtitle: buildSubtitle(company: item.company),
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => JobDetails(job: item,),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget buildTitle({title: String}) => Text(title);

  Widget buildSubtitle({company: String}) => Text('At company');
}
