import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              child: Text(
                'Welcome to Job Finder!',
                style: TextStyle(
                  fontSize: 28,
                  color: Colors.blueAccent
                ),
              ),
              alignment: Alignment.center,
              width: double.infinity,
            ),
            RaisedButton(
              child: Text('Search For Job'),
              color: Colors.blueAccent,
              onPressed: () =>
                  Navigator.of(context).pushReplacementNamed('jobs'),
            ),
          ],
        ),
      ),
    );
  }
}
