import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import './job.dart';
import './service.dart';
import 'jobList.dart';
import 'listFilter.dart';
import 'listFilterParam.dart';

class JobListAndSearch extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return JobListAndSearchState();
  }

  JobListAndSearch();
}

class JobListAndSearchState extends State<JobListAndSearch> {
  Future<List<Job>> futureJob;
  ListFilterParam filterParam = ListFilterParam.getDefault();

  @override
  void initState() {
    super.initState();
    futureJob = fetchJobs(
      description: filterParam.description,
      location: filterParam.location,
      provider: filterParam.provider,
    );
//    WidgetsBinding.instance.addPostFrameCallback((_) {
//      _showDialog();
//    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Job List'),
      ),
      body: Center(
          child: FutureBuilder<List<Job>>(
        future: futureJob,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return JobList(snapshot.data);
          } else if (snapshot.hasError) {
            return Text("${snapshot.error}");
          }

          // By default, show a loading spinner.
          return CircularProgressIndicator();
        },
      )),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _showDialog(),
        tooltip: 'Increment',
        child: Icon(Icons.filter_list),
      ),
    );
  }

  _showDialog() async {
    await showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return new CupertinoAlertDialog(
          title: new Text('Filters'),
          actions: <Widget>[
            new CupertinoDialogAction(
              isDestructiveAction: true,
              onPressed: () {
                Navigator.of(context).pop('Cancel');
              },
              child: new Text('Cancel'),
            ),
            new CupertinoDialogAction(
              isDestructiveAction: true,
              onPressed: () {
                Navigator.of(context).pop('Filter');
              },
              child: new Text('Filter'),
            ),
          ],
          content: new SingleChildScrollView(
            child: new Material(
              child: new ListFilter(
                param: filterParam,
              ),
            ),
          ),
        );
      },
      barrierDismissible: false,
    );
  }
}
