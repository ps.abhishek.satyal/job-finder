import 'package:flutter/material.dart';

import 'listFilterParam.dart';

class ListFilter extends StatefulWidget {
  ListFilter({
    Key key,
    this.param,
  }) : super(key: key);

  final ListFilterParam param;

  @override
  ListFilterState createState() => new ListFilterState();
}

class ListFilterState extends State<ListFilter> {
  @override
  void initState() {
    super.initState();
  }

  _getContent() {
    return new Column(
      children: <Widget>[
        TextField(
          controller: TextEditingController()..text = widget.param.description,
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: 'Enter a title',
          ),
        ),
        TextField(
          controller: TextEditingController()..text = widget.param.location,
//          onChanged:(value) => setState(() {
//            widget.param.location = value;
//          }),
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: 'Enter a Location',
          ),
        ),
        DropdownButton<int>(
          value: widget.param.provider,
          icon: Icon(Icons.arrow_downward),
          iconSize: 24,
          elevation: 16,
          style: TextStyle(color: Colors.deepPurple),
          underline: Container(
            height: 2,
            color: Colors.deepPurpleAccent,
          ),
          onChanged: (int newValue) {
            setState(() {
              widget.param.provider = newValue;
            });
          },
          items: <int>[0].map<DropdownMenuItem<int>>((int value) {
            return DropdownMenuItem<int>(
              value: value,
              child: Text(getLabel(0)),
            );
          }).toList(),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return _getContent();
  }

  String getLabel(int i) {
    switch (i) {
      case 0:
        return 'Job Finder';
      default:
        return 'Job Finder';
    }
  }
}
