import 'dart:convert';

import 'package:http/http.dart' as http;

import 'job.dart';

Future<List<Job>> fetchJobs(
    {description: String, location: String, provider: int}) async {
  String url = buildUrl(
      location: location, description: description, provider: provider);
  final response = await http.get(url);

  if (response.statusCode == 200) {
    var responseBody = json.decode(response.body);
    return (responseBody as List).map((e) => Job.fromJson(e)).toList();
  } else {
    throw Exception('Failed to load jobs');
  }
}

String buildUrl({description: String, location: String, provider: int}) {
  String baseUrl;
  if (provider == 0) {
    baseUrl = 'https://jobs.github.com/positions.json';
  }
  if (description != null && location != null) {
    return baseUrl + '?description=' + description + '&location=' + location;
  } else if (description == null && location != null) {
    return baseUrl + '?location=' + location;
  } else if (description != null && location == null) {
    return baseUrl + '?description=' + description;
  } else {
    return baseUrl;
  }
}

Future<Job> fetchJob({id: String}) async {
  final response =
      await http.get('https://jobs.github.com/positions/' + id + '/.json');

  if (response.statusCode == 200) {
    return Job.fromJson(json.decode(response.body));
  } else {
    throw Exception('Failed to load the job');
  }
}
