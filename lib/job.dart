import 'package:flutter/src/widgets/framework.dart';

class Job {
  String id;
  String title;
  String company;
  String location;
  String createdAt;
  String companyLogo;
  String description;

  Job(
      {this.title,
      this.company,
      this.location,
      this.createdAt,
      this.companyLogo,
      this.description,
      this.id});

  factory Job.fromJson(Map<String, dynamic> json) {
    return Job(
      id: json['id'],
      title: json['title'],
      company: json['company'],
      location: json['location'],
      createdAt: json['created_at'],
      companyLogo: json['company_logo'],
      description: json['description'],
    );
  }
}
